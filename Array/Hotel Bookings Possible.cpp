bool Solution::hotel(vector<int> &arrive, vector<int> &depart, int K) {


    sort(arrive.begin(),arrive.end());
    sort(depart.begin(),depart.end());

    int i=0,j=0;
    int cnt=0;
    while(i<arrive.size())
    {

        while(i<arrive.size()&&arrive[i]<depart[j])
        {
            K--;
            i++;
        }

        if(K<0) return 0;
        K++;
        j++;

    }

    return 1;
}


vector<vector<int> > Solution::generate(int A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details

    vector<vector<int> >m(A);

    if(A==0) return m;
    m[0].push_back(1);
    int i,j;

    for(i=1;i<A;i++)
    {
        m[i].push_back(1);
        for(j=0;j<m[i-1].size()-1;j++)
        {
            m[i].push_back(m[i-1][j]+m[i-1][j+1]);
        }
        m[i].push_back(1);
    }

    return m;

}


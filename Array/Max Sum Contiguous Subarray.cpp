int Solution::maxSubArray(const vector<int> &A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details

    int sum=INT_MIN;
    int temp=0;

    int i;
    for(i=0;i<A.size();i++)
    {
        temp+=A[i];
        if(temp>sum) sum=temp;
        if(temp<0) temp=0;
    }
    return sum;
}

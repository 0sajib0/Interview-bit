// Input : X and Y co-ordinates of the points in order.
// Each point is represented by (X[i], Y[i])
int Solution::coverPoints(vector<int> &X, vector<int> &Y) {


    int ans=0;
    int i;
    if(X.size()!=Y.size()) return 0;
    int l=X.size();
    if(l==1) return 0;

    for(i=1;i<l;i++)
    {
        ans+=max(abs(X[i]-X[i-1]),abs(Y[i]-Y[i-1]));
    }

    return ans;



}

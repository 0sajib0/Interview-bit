vector<vector<int> > Solution::diagonal(vector<vector<int> > &A) {

    int l=A[0].size();


    int k=l+l-1;

    vector<vector<int> >ans(k);

    //cout<<l<<endl;
    //return ans;

    int i,j,row,col;
    int a=0;

    for(i=0;i<l;i++)
    {
        row=0,col=i;
        while(row<l&&col>=0)
        {
            ans[a].push_back(A[row][col]);
            row++;
            col--;
        }

         a++;
    }

    //cout<<l<<endl;
    //return ans;

    for(j=1;j<l;j++)
    {
        row=j,col=l-1;

         while(row<l&&col>=0)
        {
            ans[a].push_back(A[row][col]);
            row++;
            col--;
        }

         a++;


    }

    return ans;

}


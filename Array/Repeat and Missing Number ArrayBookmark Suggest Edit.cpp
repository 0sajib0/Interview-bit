vector<int> Solution::repeatedNumber(const vector<int> &A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details


int n=A.size(),i;
std::vector<int>ans;
int p,q;

long long sum=(long long)n*(n+1)/2;
for(i=0;i<n;i++)
{
    sum-=A[i];
}

long long ab=(long long)n*(n+1)*(long long)(2*n+1)/6;

for(i=0;i<n;i++)
{
    ab-=(long long)A[i]*A[i];
}

ab=ab/sum;

int a=(ab+sum)/2;
int b=ab-a;
int flg=0;

for(i=0;i<n;i++)
{
    if(a==A[i]) flg=1;
}

if(flg==1)
{
    ans.push_back(a);
ans.push_back(b);
}
else 
{
    ans.push_back(b);
ans.push_back(a);
}

return ans;


    
}

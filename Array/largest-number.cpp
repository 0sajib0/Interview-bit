bool cmp(string a,string b)
{
    string s1=a+b;
    string s2=b+a;
    return s1<s2;

}

string Solution::largestNumber(const vector<int> &A)
{
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details

    vector<string>v;
    int i,j,k;
    string s;
    for(i=0; i<A.size(); i++)
    {
        int a=A[i];
        s="";
        if(a==0)
        {
            v.push_back("0");
            continue;
        }
        char c;
        while(a>0)
        {
            c=a%10+'0';
            a/=10;
            s=c+s;
        }

        v.push_back(s);
    }

    sort(v.begin(),v.end(),cmp);

    i=v.size()-1;
    while(i>=0&&v[i]=="0")
    {
        i--;
    }

    if(i==-1) return "0";
    s="";

    while(i>=0)
    {
        s+=v[i];
        i--;
    }
    return s;



}


vector<string> Solution::fizzBuzz(int A) {
    vector<string>vec;
    int i,j;
    string s;
    for(i=1;i<=A;i++)
    {
        if(i%3==0&&i%5==0)
        {
            vec.push_back("FizzBuzz");
        }
        else if(i%3==0)
        {
            vec.push_back("Fizz");
        }
        else if(i%5==0)
        {
            vec.push_back("Buzz");
        }
        else
        {
            int a=i;
            char c;
            s="";
            while(a>0)
            {
                c=a%10+'0';
                s=c+s;
                a/=10;
            }
            vec.push_back(s);
        }
    }

    return vec;
}


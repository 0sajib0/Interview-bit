bool Solution::isPalindrome(int A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details


    int n=A;
    int reversnumber=0;

    while(n>0)
    {
        reversnumber=reversnumber*10+n%10;
        n/=10;
    }

    return reversnumber==A;
}


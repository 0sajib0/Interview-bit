int Solution::titleToNumber(string A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details

    long long an=0;
    int i;
    for(i=0;i<A.length();i++)
    {
        an = an*26+A[i]-'A'+1;
    }
    return an;
}


bool Solution::isPower(int A) {


    int i,j;
    if(A==1) return true;

    for(i=2;i<=32;i++)
    {
        for(j=2;pow(j,i)<=A;j++)
        {
            int a=pow(j,i);
            //if(a>A) break;
            if(a==A) return true;
        }
    }

    return false;
}

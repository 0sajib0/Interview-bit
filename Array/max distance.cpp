int Solution::maximumGap(const vector<int> &A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details


    int mx=-1;
    //if(A.size()==1) return 0;

    int i,j;
    int n=A.size();
    int minArry[n],maxArry[n];
    minArry[0]=A[0];

    for(i=1;i<A.size();i++)
    {
           minArry[i]=min(A[i],minArry[i-1]);
    }
    maxArry[n-1]=A[n-1];

    for(i=n-2;i>=0;i--)
    {
             maxArry[i]=max(A[i],maxArry[i+1]);
    }

    for(i=0,j=0;i<n&&j<n;)
    {
        if(minArry[i]<=maxArry[j])
        {
            mx=max(mx,j-i);
            j++;
        }
        else i++;
    }


    return mx;

}


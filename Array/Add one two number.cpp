vector<int> Solution::plusOne(vector<int> &A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details


    int l=A.size();
    int i;
    int cary=0;
    long long n=0;
    A[l-1]++;
    vector<int>v,ans;

    for(i=l-1;i>=0;i--)
    {
        A[i]+=cary;
        cary=A[i]/10;
        //A[i]%=10;
        v.push_back(A[i]%10);
    }
    if(cary==1)
    {
        v.push_back(1);
    }
    //i=0;
    i=v.size()-1;
    while(v[i]==0) i--;

    for(int j=i;j>=0;j--) ans.push_back(v[j]);


    return ans;
}

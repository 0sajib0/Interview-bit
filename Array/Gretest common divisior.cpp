int Solution::gcd(int A, int B) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details

    if(B<A)
    {
        A+=B;
        B=A-B;
        A=A-B;
    }

    while(A>0)
    {
        int r=B%A;
        B=A;
        A=r;

    }

    return B;
}

